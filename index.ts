import axios, { AxiosResponse } from 'axios';

const COMMENTS_URL = 'https://jsonplaceholder.typicode.com/comments';

type Response = {
  id: number;
  email: string;
};

async function getData<T>(url: string): Promise<T> {
  return new Promise<T>((resolve, reject) => {
    axios
      .get(url)
      .then((response: AxiosResponse<T>) => {
        resolve(response.data);
      })
      .catch((error: Error) => {
        reject(error.message || error);
      });
  });
}

getData<Response[]>(COMMENTS_URL).then((data: Response[]) => {
  data.forEach((item: Response) => {
    console.log(`ID: ${item.id}, Email: ${item.email}`);
  });
});

/**
 * ID: 1, Email: Eliseo...
 * ID: 2, Email: Jayne_Kuhic...
 * ID: 3, Email: Nikita...
 * ID: 4, Email: Lew...
 * ...
 */
